// FadeIn.tsx
import { useSpring, animated, config } from 'react-spring';
import React from 'react';

interface FadeInProps {
	delay?: number; // in milliseconds
	duration?: number; // in milliseconds
	distance?: number; // in pixels, for the vertical movement
	children: React.ReactNode;
}

const FadeIn: React.FC<FadeInProps> = ({ delay = 0, duration = 400, distance = 20, children }) => {
	const animationProps = useSpring({
		from: { opacity: 0, transform: `translate3d(0, ${distance}px, 0)` },
		to: { opacity: 1, transform: 'translate3d(0, 0, 0)' },
		delay: delay,
		config: {
			...config.gentle,
			// ...config.wobbly,
			duration: duration,
		},
	});

	return <animated.div style={animationProps}>{children}</animated.div>;
};

export default FadeIn;
